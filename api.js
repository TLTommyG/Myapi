'use strict'

const restify = require('restify')
const server = restify.createServer()

const weatherapi = require('./modules/weatherqueryhandler')
const database = require('./modules/databasehandler')

server.use(restify.fullResponse())
server.use(restify.queryParser())
server.use(restify.bodyParser())

server.get('/weatherquery', weatherapi.doWeatherSearch)
server.get('/forecast', weatherapi.doWeatherSearch)

server.post('/database', database.add) //add new item to database
server.put('/database', database.update) //update database
server.del('/database', database.remove)//remove item from database

const port = process.env.Port || 8080

server.listen(port, err => console.log(err || `App running on port ${port}`))


const status = {
	'ok': 200,
	'created': 201,
	'noContent': 204,
	'notModified': 304,
	'badRequest': 400,
	'unauthorised': 401,
	'notFound': 404
}
