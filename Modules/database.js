'use strict'

const schema = require('./databaseschema')


/**
*Adds data to mongo database
*@param  {string} function - results of function
*@returns {string} result after add
*/

exports.add = function(results, callback) {
	const weatherdata = new schema.WS(results)

	weatherdata.save( (err, weatherdata) => {
		if (err) {
			console.log(err)
			callback(err)
		}
		callback(null, results)
	})
}

/**
*count how many documents are saved in database
*@param {string} callback
*@returns {string} potential error
*/

exports.count = callback => {
	schema.WS.count({}, (err, count) => {
		if (err) callback(err)
		callback (null, count)
	})
}

/**
*update a file in database by id
*@param {string} callback
*@returns {string} potential error
*/

exports.update = (callback) => {
	schema.WS.findByIdAndUpdate('5851681b2ee98ed06393d877' ,{ lat: '22.3333'}, {new: true} , (err, results) => {
        	if (err) {
        	callback(err)
        	}
   	})
}

/**
*delete function that removes a schema in database
*@param {string} callback
*@returns {string} potential error
*/

exports.remove = callback => {
	schema.WS.remove({}, (err, remove) => {
		if (err) callback(err)
		callback (null, remove)
	})
}

