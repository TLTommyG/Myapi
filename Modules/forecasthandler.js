'use strict'

const forecast = require('forecast')

exports.doWeatherSearch = (req, res, next) => {

	const lat = req.query.lat
	const lng = req.query.lng

	forecast.doWeatherSearch (lat,lng,(err, result) => {

		if (err) return res.send(501, err)
		return res.send(result)
	})
}
