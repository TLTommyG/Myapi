'use strict'

const database = require('./database')

exports.add = (req, res) => {
	const weatherdata = req.body

	database.add(weatherdata, (err, result) => {
		if (err) return res.send(400, err)
		return res.send(result)
	})
}

exports.update = function update_(req, res, next) {
	database.update((err, result) => {
		if(err) return res.send(501, err)
		res.send(501, {message: 'data updated'})
	})
}


exports.remove = function remove_(req, res) {
	database.remove((err, result) => {
		if (err) return res.send(501, err)
		res.send(501, {message: 'data deleted'})
	})
}
