'use strict'


const request = require('request')
const database = require('./database')

/**
*get request for data from body with database.add function implementation
*@params {string} - lat, lng inputs
*@returns {string} data from api url
*/

exports.doWeatherSearch = (lat, lng, callback) => {


	const url = `https://api.darksky.net/forecast/a6b8714609df544d8447cfdf46b40843/${lat},${lng}`

	request.get(url, function(error, response, body){
		if (!error && response.statusCode === 200){
			const weatherdata = []
			const results = JSON.parse(body).currently

			/*for (let i=0; i < results.length; i++) {
				const weather = {

					currently: results[i].volumeInfo.currently,
					minutely: results[i].volumeInfo.minutely,
					hourly: results[i].volumeInfo.hourly,
					daily: results[i].volumeInfo.daily,
					alerts: results[i].volumeInfo.alerts,
					flags: results[i].volumeInfo.flags


				}*/

			weatherdata.push(results)

/* implement add function to store data from get request */

			database.add(weatherdata, (err, result) => {
				if (err) {
					console.log(err)
					return callback ({message: 'Problem with Weather API query', error: error, statusCode: response.statusCode})
				}
				console.log(result)
				return callback(null, weatherdata)
			})
		}

	})

}
